
#include <iostream>
#include <fstream>

using namespace std;

int naiwny(){
    int n,m,a,b,wynik=0,wynikmax=0,tmpx,tmpy,x,y,op=0;

    ifstream wejscie;
    ofstream wyjscie;
    wejscie.open("in.txt");
    wyjscie.open("out.txt");
    wejscie>>m>>n;
    int** tab = new int*[n];
    for(int i = 0; i < n; i++)
        tab[i] = new int[m];
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            wejscie>>tab[i][j];
        }
    }
    wejscie>>b>>a;

    for(int i=0;i<=n-a;i++){
        for(int j=0;j<=m-b;j++){
            for(int k=0;k<a;k++){
                for(int l=0;l<b;l++){
                    wynik+=tab[k+i][l+j];
                    op++;
                }
            }
            if(wynik>wynikmax){
                    wynikmax=wynik;
                    x=i+1;
                    y=j+1;
            }
            wynik=0;
        }
    }
    wyjscie<<wynikmax<<endl<<x<<" "<<y;
return op;
}


int optymalny(){

    int n,m,a,b,wynik=0,wynikmax=0,op=0,x,y;

    ifstream wejscie;
    ofstream wyjscie;
    wejscie.open("in.txt");
    wyjscie.open("outopt.txt");
    wejscie>>m>>n;
    int** tab = new int*[n]; // tworzenie tablicy o wymiarach z pliku
    for(int i=0;i<n;i++)
        tab[i] = new int[m];

    for(int i=0;i<n;i++){ // wypełnianie tablicy danymi z pliku
        for(int j=0;j<m;j++){
            wejscie>>tab[i][j];
        }
    }

    wejscie>>b>>a;

    int** suma = new int*[n+1]; // tworzenie tablicy pomoczniczej
    for(int i=0;i<n+1;i++){
        suma[i] = new int[m+1];
    }

    for(int i=0;i<n+1;i++){ // zerowanie pierwszego wiersza i pierwszej kolumny
        suma[i][0] = 0;
        for(int j=0;j<m+1;j++){
            suma[0][j] = 0;
            op++;
        }
    }

    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            suma[i+1][j+1] = suma[i][j+1] + suma[i+1][j] - suma[i][j] + tab[i][j];
            op++;
        }
    }

    for(int i=0;i<=n-a;i++){
        for(int j=0;j<=m-b;j++){
            wynik = suma[i+a][j+b] - suma[i][j+b] - suma[i+a][j] + suma[i][j];
            if(wynik>wynikmax){
                    wynikmax=wynik;
                    x=i+1;
                    y=j+1;
            }
            wynik=0;
            op++;
        }
    }

    wyjscie<<wynikmax<<endl<<x<<" "<<y;
    return op;
}


int main()
{
    cout<<endl<<"operacje algorytm optymalny: "<<optymalny();
    //cout<<endl<<"operacje algorytm naiwny: "<<naiwny();
    return 0;
}
